import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SpotifySupportService {
  private token =
    'Bearer ' +
    'BQCbGvM2QJIZ6p5FEgZFRP7TjtAACG6lKhqIZUm5WlTvHEgRBh6TYnzFe75rnA7_qSTNJzsiic5srUcIvxUaVbFr1JWCRP2NwNJIrfBYQegLgsOrMcNQmCggMM_YDcBnCEy3g2Cu4Gdhkdc';

  private httpOptions = {
    headers: new HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: this.token,
    }),
  };
  constructor(private http: HttpClient) {}

  public getArtistByName(name: string): Observable<any> {
    const firstTryArtistbyName = `https://api.spotify.com/v1/search?q=${name}&type=artist&limit=1`;
    return this.http
      .get(firstTryArtistbyName, this.httpOptions)
      .pipe(map((res) => res));
  }

  public searchArtist() {
    /* 
    const firstTryArtistbyName = `https://api.spotify.com/v1/search?q=${name}&type=artist&limit=1`; */
  }

  private handleError(error: Response) {
    return Observable.throw(error || 'Server Error');
  }
}
