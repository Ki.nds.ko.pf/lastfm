import { Artist } from '../models/Artist';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiLastfmService {
  API_KEY_LASTFM = '298c85d20b3d1fe5748e9772eab13a0f';
  API_KEY_THEMOVIEDB = '298c85d20b3d1fe5748e9772eab13a0f';
  MAGIC_NUM_LIMIT = '12';

  constructor(private http: HttpClient) {}

  searchMusic(artistName: string, queryType: string) {
    return this.http.get(
      `http://ws.audioscrobbler.com/2.0/?method=artist.${queryType}&artist=${artistName}&api_key=${this.API_KEY_LASTFM}&format=json`
    );
  }

  searchMusicMy(artistName: string, queryType: string) {
    return this.http.get(
      `https://ws.audioscrobbler.com/2.0/?method=artist.search&artist=${artistName}&limit=10&api_key=${this.API_KEY_LASTFM}&format=json`
    );
  }

  searchArtist(artistName: string): Observable<any> {
    return this.http
      .get(
        `http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${artistName}&api_key=${this.API_KEY_LASTFM}&format=json`
      )
      .pipe(map((res) => res));
  }

  getTopArtistsInGeo(geo: string) {
    let localArtists = [] as Artist[];
    this.http
      .get(
        `https://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=${geo}&limit=${this.MAGIC_NUM_LIMIT}&api_key=${this.API_KEY_LASTFM}&format=json`
      )
      .subscribe((res: any) => {
        console.log(res.topartists.artist);
        res.topartists.artist.forEach((artist) => {
          let lastfmArtistGeo = new Artist();
          lastfmArtistGeo.setLastFmValues(
            artist.name,
            artist.images,
            artist.listeners,
            artist.playcount,
            artist.summary,
            artist.lUrl,
            artist.mbid
          );
          return localArtists.push(lastfmArtistGeo);
        });
      });
    return localArtists;
  }
}

/* name: string,
    lImages: [],
    sImage: {
      height: number;
      url: string;
      width: number;
    }[],
    listeners: number,
    playcount: number,
    summary: string,
    popularity: number,
    lUrl: string,
    sUrl: string,
    uri: string,
    followers: number,
    genres: string[],
    mbid: string,
    spid: string */
