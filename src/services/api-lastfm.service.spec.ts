import { TestBed } from '@angular/core/testing';

import { ApiLastfmService } from './api-lastfm.service';

describe('ApiLastfmService', () => {
  let service: ApiLastfmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiLastfmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
