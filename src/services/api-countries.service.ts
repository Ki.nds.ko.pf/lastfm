import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from '../models/Country';

@Injectable({
  providedIn: 'root',
})
export class ApiCountriesService {
  countrys: Country[] = [];
  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  getAllCountries() {
    let localCountrys: Array<Country> = [];
    this.http
      .get('http://countryapi.gear.host/v1/Country/getCountries')
      .subscribe((res: any) => {
        res.Response.forEach((element) => {
          localCountrys.push(
            new Country(element.Name, element.NativeName, element.Flag)
          );
        });
      });

    console.log(localCountrys);
    return localCountrys;
  }

  getDE() {
    let localCountrys: Array<Country> = [];
    this.http
      .get('http://countryapi.gear.host/v1/Country/getCountries?pName=Germany')
      .subscribe((res: any) => {
        res.Response.forEach((element) => {
          localCountrys.push(
            new Country(element.Name, element.NativeName, element.Flag)
          );
        });
      });

    return localCountrys;
  }
}
