import { TestBed } from '@angular/core/testing';

import { SpotifySupportService } from './spotify-support.service';

describe('SpotifySupportService', () => {
  let service: SpotifySupportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpotifySupportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
