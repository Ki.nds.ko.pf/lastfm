export class Artist {
  private _name: string;
  private _lImages: [];
  private _sImage: {
    height: number;
    url: string;
    width: number;
  }[];
  private _listeners: number;
  private _playcount: number;
  private _summary: string;
  private _popularity: number;
  private _lUrl: string;
  private _sUrl: string;
  private _uri: string;
  private _followers: number;
  private _genres: string[];
  private _mbid: string;
  private _spid: string;

  constructor() {}

  public setLastFmValues(
    name: string,
    lImages,
    listeners: number,
    playcount: number,
    summary: string,
    lUrl: string,
    mbid: string
  ) {
    this._name = name;
    this._lImages = lImages;
    this._listeners = listeners;
    this._playcount = playcount;
    this._summary = summary;
    this._lUrl = lUrl;
    this._mbid = mbid;
  }

  public setSpotifyValues(
    _sImage: { height: number; url: string; width: number }[],
    _popularity: number,
    _sUrl: string,
    _uri: string,
    _followers: number,
    _genres: string[],
    _spid: string
  ) {
    this._sImage = _sImage;
    this._popularity = _popularity;
    this._sUrl = _sUrl;
    this._uri = _uri;
    this._followers = _followers;
    this._genres = _genres;
    this._spid = _spid;
  }

  /*   mergeLastFmArtist(artist: Artist) {
    this.currentArtist.setLastFmValues(
      artist.name,
      artist.lImages,
      artist.listeners,
      artist.playcount,
      artist.summary,
      artist.lUrl,
      artist.mbid
    );
  }

  mergeSpotifyArtist(artist: Artist) {
    this.currentArtist.setSpotifyValues(
      artist.sImage,
      artist.popularity,
      artist.sUrl,
      artist.uri,
      artist.followers,
      artist.genres,
      artist.spid
    );
  } */

  //method
  displayAsString(): void {
    console.log(this);
  }

  getFullArtist() {}

  get name() {
    return this._name;
  }

  set name(val: string) {
    this._name = val;
  }

  get lImages() {
    return this._lImages;
  }

  set lImages(val: []) {
    this._lImages = val;
  }

  get sImage() {
    return this._sImage;
  }

  set sImage(
    val: {
      height: number;
      url: string;
      width: number;
    }[]
  ) {
    this._sImage = val;
  }

  get listeners() {
    return this._listeners;
  }

  set listeners(val: number) {
    this._listeners = val;
  }

  get playcount() {
    return this._playcount;
  }

  set playcount(val: number) {
    this._playcount = val;
  }

  get summary() {
    return this._summary;
  }

  set summary(val: string) {
    this._summary = val;
  }

  get popularity() {
    return this._popularity;
  }

  set popularity(val: number) {
    this._popularity = val;
  }

  get lUrl() {
    return this._lUrl;
  }

  set lUrl(val: string) {
    this._lUrl = val;
  }

  get sUrl() {
    return this._sUrl;
  }

  set sUrl(val: string) {
    this._sUrl = val;
  }

  get uri() {
    return this._uri;
  }

  set uri(val: string) {
    this._uri = val;
  }

  get followers() {
    return this._followers;
  }

  set followers(val: number) {
    this._followers = val;
  }

  get genres() {
    return this._genres;
  }

  set genres(val: string[]) {
    this._genres = val;
  }

  get mbid() {
    return this._mbid;
  }

  set mbid(val: string) {
    this._mbid = val;
  }

  get spid() {
    return this._spid;
  }

  set spid(val: string) {
    this._spid = val;
  }
}
