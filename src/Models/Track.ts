export class Track {
  public name: string;
  public listeners: number;
  public rank: number;
  public url: string;
  constructor(name: string, listeners: number, rank: number, url: string) {}
}
