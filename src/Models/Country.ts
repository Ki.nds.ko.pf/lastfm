export class Country {
  public name: string;
  public nativeName: string;
  public flagSVG: string;
  constructor(name: string, nativeName: string, flagSVG: string) {
    this.name = name;
    this.nativeName = nativeName;
    this.flagSVG = flagSVG;
  }
}
