import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ApiCountriesService } from '../../services/api-countries.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Country } from 'src/models/Country';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
})
export class CountriesComponent implements OnInit {
  @Output()
  public activeCountry: EventEmitter<Country> = new EventEmitter();
  events: string[] = [];
  mode = new FormControl('over');
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some((h) =>
    h.test(window.location.host)
  );

  stateCtrl = new FormControl();
  filteredStates: Observable<Country[]>;
  selected: Country = new Country(
    'Germany',
    'Deutschland',
    'https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlags/deu.svg'
  );
  countrys: Country[] = [];

  constructor(private apiCountriesService: ApiCountriesService) {
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      startWith(''),
      map((state) =>
        state ? this._filterStates(state) : this.countrys.slice()
      )
    );
  }

  ngOnInit(): void {
    console.log('testOnInit');
    this.activeCountry.emit(this.selected);
    let localCountrys: Array<Country> = [];

    localCountrys = this.apiCountriesService.getAllCountries();
    this.countrys = localCountrys;
  }

  private _filterStates(value: string): Country[] {
    const filterValue = value.toLowerCase();

    return this.countrys.filter(
      (state) =>
        state.name.toLowerCase().indexOf(filterValue) === 0 ||
        state.nativeName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  onChangeCountries() {
    console.log(this.stateCtrl.value);

    let current;
    current = this.countrys.find(
      (country) => country.name === this.stateCtrl.value
    );
    this.selected = current;

    console.log(
      this.countrys.find((country) => country.name === this.stateCtrl.value)
    );

    this.activeCountry.emit(current);
  }
}
