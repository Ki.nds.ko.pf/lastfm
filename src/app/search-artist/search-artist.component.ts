import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Artist } from 'src/models/Artist';
import { ApiLastfmService } from 'src/services/api-lastfm.service';

@Component({
  selector: 'app-search-artist',
  templateUrl: './search-artist.component.html',
  styleUrls: ['./search-artist.component.scss'],
})
export class SearchArtistComponent implements OnChanges {
  value = 'Clear me';
  searchStr: string;

  @Input() activeCountryForArtist: string;
  @Output() activeTopArtists: EventEmitter<Array<Artist>> = new EventEmitter();
  currentCountry: string;

  constructor(private apiLastfmService: ApiLastfmService) {}

  searchMusicMy() {
    let localArtists = [] as Artist[];
    this.apiLastfmService
      .searchMusicMy(this.searchStr, 'getinfo')
      .subscribe((res: any) => {
        console.log(res.results.artistmatches.artist);
        res.results.artistmatches.artist.forEach((artist) => {
          let lastfmArtistGeo = new Artist();
          lastfmArtistGeo.setLastFmValues(
            artist.name,
            artist.images,
            artist.listeners,
            artist.playcount,
            artist.summary,
            artist.lUrl,
            artist.mbid
          );
          localArtists.push(lastfmArtistGeo);
        });
      });
    this.activeTopArtists.emit(localArtists);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.activeCountryForArtist.currentValue !=
      changes.activeCountryForArtist.previousValue
    ) {
      console.log(changes);
      this.activeTopArtists.emit(
        this.getTopArtistsInGeo(changes.activeCountryForArtist.currentValue)
      );
    }
  }

  getTopArtistsInGeo(country: string): Artist[] {
    console.log('wurde geändert');
    let localArtists = [] as Artist[];
    localArtists = this.apiLastfmService.getTopArtistsInGeo(country);
    console.log(localArtists);
    return localArtists;
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }
}
