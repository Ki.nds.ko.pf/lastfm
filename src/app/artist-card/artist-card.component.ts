import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Artist } from 'src/models/Artist';
import { SpotifySupportService } from '../../services/spotify-support.service';

@Component({
  selector: 'app-artist-card',
  templateUrl: './artist-card.component.html',
  styleUrls: ['./artist-card.component.scss'],
})
export class ArtistCardComponent implements OnInit, OnChanges {
  @Input() artist: Artist;
  currentArtist = new Artist();
  artistImg: any = {
    height: 0,
    url:
      'https://lastfm.freetls.fastly.net/i/u/34s/2a96cbd8b46e442fc41c2b86b821562f.png',
    width: 0,
  };

  constructor(private spotifySupportService: SpotifySupportService) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log('Artist Card');
    this.currentArtist.displayAsString();
    this.spotifySupportService
      .getArtistByName(changes.artist.currentValue.name)
      .subscribe(
        (res) => {
          let artist = new Artist();
          artist.sImage = res.artists.items[0].images;
          artist.uri = res.artists.items[0].uri;
          artist.genres = res.artists.items[0].genres;
          artist.spid = res.artists.items[0].id;
          artist.followers = res.artists.items[0].followers.total;
          artist.sUrl = res.artists.items[0].external_urls.spotify;
          artist.popularity = res.artists.items[0].popularity;
          this.mergeSpotifyArtist(artist);
          this.mergeLastFmArtist(changes.artist.currentValue);
          this.artistImg = artist.sImage[0];
        },
        (err) => {
          console.log(err);
        }
      );
  }

  mergeLastFmArtist(artist: Artist) {
    this.artist.lUrl = artist.lUrl;
    this.currentArtist.setLastFmValues(
      artist.name,
      artist.lImages,
      artist.listeners,
      artist.playcount,
      artist.summary,
      artist.lUrl,
      artist.mbid
    );
  }

  mergeSpotifyArtist(artist: Artist) {
    this.artist.uri = artist.uri;
    this.artist.sUrl = artist.sUrl;
    this.artist.genres = artist.genres;
    this.currentArtist.setSpotifyValues(
      artist.sImage,
      artist.popularity,
      artist.sUrl,
      artist.uri,
      artist.followers,
      artist.genres,
      artist.spid
    );
  }
}
