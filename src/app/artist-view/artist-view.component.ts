import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Artist } from 'src/models/Artist';
import { ApiLastfmService } from 'src/services/api-lastfm.service';
import { SpotifySupportService } from 'src/services/spotify-support.service';

@Component({
  selector: 'app-artist-view',
  templateUrl: './artist-view.component.html',
  styleUrls: ['./artist-view.component.scss'],
})
export class ArtistViewComponent implements OnInit {
  @Output()
  public closedArtistView: EventEmitter<boolean> = new EventEmitter();
  @Input() artist: Artist;
  artistImg;
  bio;
  showMore1 = false;

  constructor(
    private apiLastfmService: ApiLastfmService,
    private spotifySupportService: SpotifySupportService
  ) {}

  ngOnInit(): void {
    console.log('test1');

    let bigArtistLastFm = this.apiLastfmService
      .searchArtist(this.artist.name)
      .subscribe((res) => {
        console.log(res);
        let lastfmArtist = new Artist();
        lastfmArtist.setLastFmValues(
          res.artist.name,
          res.artist.images,
          res.artist.listeners,
          res.artist.playcount,
          res.artist.summary,
          res.artist.lUrl,
          res.artist.mbid
        );
        console.log(lastfmArtist);
        this.bio = res.artist.bio;
      });
    let spotifyArtist = this.spotifySupportService
      .getArtistByName(this.artist.name)
      .subscribe(
        (res) => {
          console.log(res);
          this.artistImg = res.artists.items[0].images[0];
        },
        (err) => {
          console.log(err);
        }
      );

    // Spotify daten abrufen
    // lastfm daten abrufen

    // top 5 albums
    // top 5 tracks
  }

  close() {
    console.log(this.artist);
    this.closedArtistView.emit(true);
  }
}
