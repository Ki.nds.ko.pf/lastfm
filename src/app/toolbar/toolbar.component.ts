import {
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';
import { Country } from 'src/models/Country';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnChanges {
  @Output()
  public activeCountryInToolbar: EventEmitter<Country> = new EventEmitter();
  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  /* https://wuschools.com/ngonchanges-example-angular-7/ */

  getOutputCountry(selected: Country): void {
    console.log('toolbar' + selected);
    this.activeCountryInToolbar.emit(selected);
  }
}
