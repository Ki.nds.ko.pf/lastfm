import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Country } from 'src/models/Country';
import { Artist } from 'src/models/Artist';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'my-lastfm';
  activeCountryForArtist: string;
  activeTopArtistForCards: Artist[];

  // NavbarComponent
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  // CountriesComponent
  @Output()
  public activeCountry: EventEmitter<Country> = new EventEmitter();
  country: Country;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit() {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
      map((e) => true),
      shareReplay()
    );
  }

  getOutputCountry(selected: Country): void {
    console.log(selected);
    this.activeCountry.emit(selected);
    this.country = selected;
  }

  getOutputArtists(selected: Artist[]): void {
    console.log('Artist ist zur App gekommen');
    this.activeTopArtistForCards = selected;
  }
}
