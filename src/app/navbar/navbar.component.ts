import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Country } from 'src/models/Country';
import { NumberFormatStyle } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  @Output()
  public activeCountryInToolbar: EventEmitter<Country> = new EventEmitter();
  country: Country;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit() {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
      map((e) => true),
      shareReplay()
    );
  }

  getOutputCountry(selected: Country): void {
    console.log(selected);
    this.activeCountryInToolbar.emit(selected);
    this.country = selected;
  }
}
